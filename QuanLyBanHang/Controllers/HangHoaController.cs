﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuanLyBanHang.Models;
using System.Data.Entity;
namespace QuanLyBanHang.Controllers
{
    public class HangHoaController : Controller
    {
        QLBHEntities qlbh = new QLBHEntities();
        // GET: HangHoa
        public ActionResult Index()
        {
            return View(qlbh.HangHoas.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(HangHoa hangHoa)
        {
            qlbh.HangHoas.Add(hangHoa);
            qlbh.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            HangHoa hangHoa = qlbh.HangHoas.Find(id);
            if(hangHoa != null)
            {
                return View(hangHoa);
            }
            return Content("Không có mặt hàng có id = " + id);
        }
        [HttpPost]
        public ActionResult Update(HangHoa hangHoa)
        {
            qlbh.Entry(hangHoa).State = EntityState.Modified;
            qlbh.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            
            HangHoa hangHoa = qlbh.HangHoas.Find(id);
            if (hangHoa != null)
            {
                qlbh.HangHoas.Remove(hangHoa);
                qlbh.SaveChanges();
                return RedirectToAction("Index");
            }
            return Content("Không có mặt hàng có id = " + id);

        }

        public ActionResult Details(int id)
        {
            
            HangHoa hangHoa = qlbh.HangHoas.Find(id);
            if (hangHoa != null)
            {
                return View(hangHoa);
            }
            return Content("Không có mặt hàng có id = " + id);
                
        }







    }
}